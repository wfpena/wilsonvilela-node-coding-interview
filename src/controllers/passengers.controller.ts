import {
    JsonController,
    Get,
    Param,
    Put,
    Post,
    Body,
} from 'routing-controllers';
import { PassengersService } from '../services/passengers.service';

@JsonController('/passengers', { transformResponse: false })
export default class FlightsController {
    private _passengersService: PassengersService;

    constructor() {
        this._passengersService = new PassengersService();
    }

    @Post('')
    async addPassenger(
        @Body()
        passenger: {
            name: string;
            flightCode: string;
        },
    ) {
        return {
            status: 200,
            data: await this._passengersService.addPassenger(passenger),
        };
    }
}
