import { Database } from '../databases/database_abstract';
import { DatabaseInstanceStrategy } from '../database';

export class PassengersService {
    private readonly _db: Database;

    constructor() {
        this._db = DatabaseInstanceStrategy.getInstance();
    }

    public async addPassenger(passenger: {
        name: string;
        flightCode: string;
    }) {
        return this._db.addPassenger(passenger);
    }
}
