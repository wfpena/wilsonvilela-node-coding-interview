import mongoose, { Schema } from 'mongoose';

interface Passenger {
    name: string;
}

const schema = new Schema<Passenger>(
    {
        name: { required: true, type: String },
        flight : { type: Schema.Types.ObjectId, ref: 'Flight' },
    },
    { timestamps: true },
);

export const PassengersModel = mongoose.model('Passengers', schema);
